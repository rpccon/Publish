﻿<%@ Page Title="Consultar Estudiantes" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Consultar_Estudiantes.aspx.cs" Inherits="SICRER_Web_Application.Consultar_Estudiantes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/Site.css" rel="stylesheet" />
    <script src="Scripts/site.js" type="text/javascript"></script>

    <div class="col-md-3 space-up">
        <ul class="nav nav-pills nav-stacked well">
            <li><a href="Consultar_Revisiones.aspx"> Consultar Revisiones</a></li>
            <li><a href="Configurar_Grupos_Admin.aspx"> Consultar Grupos</a></li>
            <li class="active"><a href="Consultar_Estudiantes.aspx"> Consultar Estudiantes</a></li>
            <li><a href="Consultar_Activos_Inactivos.aspx">Consultar Activoss/Inactivos</a></li>
        </ul>
    </div>
    <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
     	<ContentTemplate>

            <div class="col-md-9 space-up">
                <div>
                    <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
                        <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
                        <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </asp:Panel>

                    <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
                        <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
                        <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </asp:Panel>

                    <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
                        <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
                        <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </asp:Panel>    
                </div>
                <div class="form-inline">
                    <asp:DropDownList ID="ddResidencia" runat="server" DataTextField="NUMERO_RESIDENCIA" CssClass="selectpicker form-control" 
                        Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddResidencia_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>

                <br /> <br />
                <asp:GridView ID="gvEstudiantes" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered"
                    Visible='<%# ((System.Data.DataTable)gvEstudiantes.DataSource).Rows.Count == 0 ? false : true %>'
                    OnDataBound="gvEstudiantes_DataBound">
                    <Columns>
                        <asp:BoundField DataField="CUARTO" HeaderText="CUARTO" SortExpression="CUARTO" HtmlEncode="false" ReadOnly="true"></asp:BoundField>
                        <asp:BoundField DataField="CUARTO" HeaderText="Nº PERSONAS" SortExpression="Nº PERSONAS" HtmlEncode="false" ReadOnly="true"></asp:BoundField>
                        <asp:BoundField DataField="CARNE" HeaderText="CARNÉ" SortExpression="CARNE" ReadOnly="true"></asp:BoundField>
                        <asp:BoundField DataField="RESIDENTE" HeaderText="RESIDENTE" SortExpression="RESIDENTE" ReadOnly="true"></asp:BoundField>
                        <asp:BoundField DataField="COORDINADOR" HeaderText="COORDINADOR" SortExpression="COORDINADOR" ReadOnly="true"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
        </ContentTemplate>
   </asp:UpdatePanel>
    <script src="Scripts/bootstrap.min.js"></script>
</asp:Content>

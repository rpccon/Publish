﻿<%@ Page Title="Registrar Estudiantes" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registrar_Estudiantes.aspx.cs" Inherits="SICRER_Web_Application.Registrar_Estudiantes" EnableEventValidation="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/personalizado.css" rel="stylesheet" />
   <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->

    <link href="Content/jquery-ui-smoothness-1.11.4.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui-smoothness-theme-1.11.4.css" rel="stylesheet" />
    <link href="Content/revisiones.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.11.4.js"></script>
    <script src="Scripts/jquery.ui.datepicker-es.js"></script>
    <script src="Scripts/revisiones.js"></script>
    <script src="Scripts/site.js"></script>       

     
    <script type="text/javascript">

        function inhabilitarRequest() {
            document.getElementById('<%=RequiredFieldValidatorResidencia.ClientID %>').enabled = false;
                    }
        function hideElement() {
            document.getElementById("gvRegistrados").style.display = "none";
        }
        function prue() {
       

           
        }

        function showElement() {
            document.getElementById("gvRegistrados").style.display = "block";
        }
        function inhabilitarRequest() {
            document.getElementById('<%=RequiredFieldValidatorApellido1.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidatorApellido2.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidatorCarne.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidatorCarrera.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidatorCoordinador.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidatorCuarto.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidatorEstado.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidatorLlaveCocina.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidatorNombre.ClientID %>').enabled = false;
            document.getElementById('<%=RequiredFieldValidatorResidencia.ClientID %>').enabled = false;
        }
    </script>

    <div class="col-md-12">
        <div class="col-md-3 space-up">
            <ul class="nav nav-pills nav-stacked well">
                <li class="active"><a href="Registrar_Estudiantes.aspx">Estudiantes</a></li>
                <li><a href="Registro_Residencias.aspx">Residencias</a></li>
            </ul>
        </div>

        <div class="col-md-7 space-up" style="align-content: center">
            <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
            <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
            <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
                <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
                <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
                <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
                <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <div id="panel_A" class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Estudiantes</div>
                </div>
                <div class="panel-body form-horizontal">
                    <asp:ValidationSummary runat="server" ID="ValidationSummary1"
                        DisplayMode="BulletList"
                        ShowMessageBox="False" ShowSummary="True" CssClass="alert alert-danger" />
                    <div class="form-group">
                        <label class="control-label col-sm-4">
                            Apellido 1
                        </label>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" CssClass="form-control" ID="tbApellido1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorApellido1"
                                runat="server"
                                ErrorMessage="Digite primer apellido" ControlToValidate="tbApellido1"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                            <asp:RegularExpressionValidator ID="ValidarApellido1" runat="server"
                                ControlToValidate="tbApellido1"
                                ErrorMessage="Entrada Inválida o Tamaño no permitido"
                                Display="Dynamic" ValidationExpression="^[a-zA-Zá-úä-üÁ-ÚÄ-Ü'.\s]{1,50}$"
                                SetFocusOnError="true" CssClass="alert-text"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">
                            Apellido 2
                        </label>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" CssClass="form-control" ID="tbApellido2" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorApellido2"
                                runat="server"
                                ErrorMessage="Digite segundo apellido" ControlToValidate="tbApellido2"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                            <asp:RegularExpressionValidator ID="ValidarApellido2" runat="server"
                                ControlToValidate="tbApellido2"
                                ErrorMessage="Entrada Inválida o Tamaño no permitido"
                                Display="Dynamic" ValidationExpression="^[a-zA-Zá-úä-üÁ-ÚÄ-Ü'.\s]{1,50}$"
                                SetFocusOnError="true" CssClass="alert-text"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-horizontal control-label col-md-4 ">
                            Nombre
                        </label>
                        <div class="col-md-5">
                            <asp:TextBox runat="server" CssClass="form-control" ID="tbNombre" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorNombre"
                                runat="server"
                                ErrorMessage="Digite el nombre" ControlToValidate="tbNombre"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text inline" />
                            <asp:RegularExpressionValidator ID="ValidarNombre" runat="server"
                                ControlToValidate="tbNombre"
                                ErrorMessage="Entrada Inválida o Tamaño no permitido"
                                Display="Dynamic" ValidationExpression="^[a-zA-Zá-úä-üÁ-ÚÄ-Ü'.\s]{1,50}$"
                                SetFocusOnError="true" CssClass="alert-text"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">
                            Carné
                        </label>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" CssClass="form-control" ID="tbCarne" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCarne"
                                runat="server"
                                ErrorMessage="Digite el carné" ControlToValidate="tbCarne"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                            <asp:RegularExpressionValidator ID="ValidarCarne" runat="server"
                                ControlToValidate="tbCarne"
                                ErrorMessage="Entrada Inválida o Carné Inválido"
                                Display="Dynamic" ValidationExpression="^[0-9\s]{4,11}$"    
                                SetFocusOnError="true" CssClass="alert-text"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">
                            Email
                        </label>
                        <div class="col-sm-5">
                            <asp:TextBox runat="server" CssClass="form-control" ID="esp_email" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                runat="server"
                                ErrorMessage="Digite el email" ControlToValidate="esp_email"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">
                            Carrera
                        </label>
                        <div class="col-sm-5">

                            <asp:DropDownList runat="server" ID="ddCarreras" DataTextField="Carrera"  CssClass="selectpicker form-control" DataValueField="Carrera">
                            </asp:DropDownList>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCarrera"
                                runat="server"
                                ErrorMessage="Seleccione carrera" ControlToValidate="ddCarreras"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">
                            Residencias
                        </label>
                        <div class="col-sm-5">

                            <asp:DropDownList runat="server" ID="ddResidencias" DataTextField="Residencia" CssClass="selectpicker form-control" DataValueField="Residencia">
                            </asp:DropDownList>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorResidencia"
                                runat="server"
                                ErrorMessage="Seleccione residencia" ControlToValidate="ddResidencias"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">
                            Cuarto
                        </label>
                        <div class="col-sm-5">

                            <asp:DropDownList runat="server" ID="ddCuartos"  CssClass="selectpicker form-control">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>8</asp:ListItem>
                                <asp:ListItem>9</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                            </asp:DropDownList>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCuarto"
                                runat="server"
                                ErrorMessage="Seleccione cuarto" ControlToValidate="ddCuartos"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">
                            Estado
                        </label>
                        <div class="col-sm-5">

                            <asp:DropDownList runat="server" ID="ddEstado" aria-expanded="true" CssClass="selectpicker form-control">
                                <asp:ListItem>Activo</asp:ListItem>
                                <asp:ListItem>Inactivo</asp:ListItem>
                            </asp:DropDownList>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorEstado"
                                runat="server"
                                ErrorMessage="Seleccione estado" ControlToValidate="ddEstado"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">
                            Llave cocina
                        </label>
                        <div class="col-sm-8">
                            <asp:RadioButtonList ID="rbLlaveCocina" ValidationGroup="valGroup1" runat="server" CssClass="rbEspacio radio" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Si" />
                                <asp:ListItem Value="No" Selected="True" />
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLlaveCocina"
                                runat="server"
                                ErrorMessage="Seleccione llave cocina" ControlToValidate="rbLlaveCocina"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                        </div>
                    </div>
                    <div id="modalAbandono" class="modal fade" role="dialog">
                        <div class="modal-dialog  ">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 id="mensH4" runat="server" class="modal-title">Estudiantes de la Revisión</h4>
                            </div>
                            <div class="modal-body">
                                <div align="center" Class="form-inline">

                                    <asp:Label ID="lbA" Height="75px" Width="200px" runat="server">¿Por qué va a dejar residencias?</asp:Label>
                                    <asp:Label ID="lbB" Height="75px" Width="200px" runat="server">  ¿Por qué va a dejar de usar la cocina?</asp:Label>
 

                                </div>
                             <div align="center" Class="form-inline">
                                    <asp:TextBox Height="75px" Width="200px" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    <asp:TextBox Height="75px" Width="200px" TextMode="MultiLine" runat="server"></asp:TextBox>
                             </div>
                               
                            </div>
                            <div class="modal-footer">
                                 <asp:LinkButton ID="perfect" runat="server" OnClientClick="prue()">Boton</asp:LinkButton>
                            
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        </div>
                   </div>

                   <div style="display:none" class="form-group">
                        <label class="control-label col-sm-4">
                            Coordinador
                        </label>
                        <div class="col-sm-8">
                            <asp:RadioButtonList ID="rbCoordinador" ValidationGroup="valGroup1" runat="server" CssClass="rbEspacio radio" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Si" />
                                <asp:ListItem Value="No" Selected="True" />
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCoordinador"
                                runat="server"
                                ErrorMessage="Seleccione coordinador" ControlToValidate="rbCoordinador"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8" style="align-content:center">
                            <asp:LinkButton runat="server" ID="linkRegistrar" CommandName="Login"
                                CssClass="btn btn-default btn-primary" Text="<span class='glyphicon glyphicon-book'> </span> Registrar" OnClick="btRegistrar_Click" />
                            <asp:LinkButton runat="server" ID="linkActualizar" CommandName="Login"
                                CssClass="btn btn-default btn-warning" Text="<span class='glyphicon glyphicon-edit'> </span> Actualizar" OnClick="btActualizar_Click" />
                        </div>
                    </div>
                </div>
               </div>
            <div id="panel_C" class="panel panel-default">
                 <div id="div_title" class="panel-heading">
                        <div class="panel-title">Desalojo de cocina o residencia</div>
                </div>
                <div id="panel_setDesalojo" align="center" class="panel-body form-horizontal">
                  <div id="subpanel_A" class="form-inline space-up space-down">
                       <asp:Label Width="120" ID="lb_askA" runat="server">¿ Por qué deja la residencias estudiantiles?</asp:Label>
                       <asp:DropDownList ID="lista_des_resi" runat="server" CssClass="selectpicker form-control" Width="125px">
                            <asp:ListItem>Va alquilar apartamento</asp:ListItem>
                            <asp:ListItem>Va a viajar desde casa</asp:ListItem>
                           <asp:ListItem>No se tiene presupuesto para pagar</asp:ListItem>
                           <asp:ListItem>Va a dejar de estudiar</asp:ListItem>
                           <asp:ListItem>Otras razones</asp:ListItem>
                        </asp:DropDownList>   
                      <asp:TextBox  ID="observ_A" runat="server"  Height="80px" Width="300px" TextMode="MultiLine"></asp:TextBox>             
                      
                       
                       <!-- <asp:DropDownList ID="DropDownList1" runat="server" CssClass="selectpicker form-control" Width="125px">
                            <asp:ListItem>Carné</asp:ListItem>
                            <asp:ListItem>Nombre</asp:ListItem>
                        </asp:DropDownList>
                   
                        <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" Text="<span class='glyphicon glyphicon-search'></span> Buscar " CssClass="btn btn-primary" OnClick="linkBuscar_Click"></asp:LinkButton>-->
                    </div>
                    <div id="subpanel_B" class="form-inline space-up space-down">
                        <asp:Label ID="lb_askB" Width="120" runat="server" >¿ Por qué va dejar de utilizar la cocina?</asp:Label>
                          <asp:DropDownList ID="drop_casos_cocina" OnSelectedIndexChanged="casos_cocina" runat="server" CssClass="selectpicker form-control" Width="125px">                      
                            <asp:ListItem Value="1">Va a comprar la comida</asp:ListItem>
                            <asp:ListItem Value="2">El horario de limpieza es incompatible</asp:ListItem>
                            <asp:ListItem Value="3">Problemas con el grupo de limpieza</asp:ListItem>
                            <asp:ListItem Value="4">Nunca se cumple el horario</asp:ListItem>
                             <asp:ListItem Value="5">Va a dejar de estudiar</asp:ListItem>
                            <asp:ListItem Value="6" >Otras razones</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="observ_B" runat="server"  Height="80px" Width="300px" TextMode="MultiLine"></asp:TextBox>
                        <asp:TextBox ID="TB_ToSave" runat="server" ></asp:TextBox>        
                        <asp:TextBox ID="globalErr" runat="server" ></asp:TextBox>    

                        <asp:TextBox ID="errA" runat="server" ></asp:TextBox> 
                        <asp:TextBox ID="errB" runat="server" ></asp:TextBox> 
                        <asp:TextBox ID="errC" runat="server" ></asp:TextBox> 
                        <asp:TextBox ID="errD" runat="server" ></asp:TextBox> 
                    </div>
                    <div id="subpanel_setResiCocina" class="form-inline space-up space-down">
                        <asp:Label ID="Labfgfgel1" Width="120" runat="server" >Indique la residencia y la cocina</asp:Label>

                        <asp:DropDownList ID="tercero" runat="server" DataTextField="Residencia" CssClass="selectpicker form-control" Width="130px">
                         </asp:DropDownList>
                          <asp:DropDownList ID="dd_ala" OnSelectedIndexChanged="casos_cocina" runat="server" CssClass="selectpicker form-control" Width="125px">                      
                                <asp:ListItem Value="1">Ala</asp:ListItem>
                                <asp:ListItem Value="2">A</asp:ListItem>
                                <asp:ListItem Value="3">B</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div  class="form-inline space-up space-down">
                        <asp:LinkButton ID="button_continuar" CssClass="btn btn-warning" OnClick="continuar_click" runat="server">Continuar</asp:LinkButton>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" RenderMode="Inline" ChildrenAsTriggers="true">
                        <ContentTemplate>
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" runat="server"  OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnRowDataBound="GvCursor_RowDataBound" AutoGenerateColumns="False" CssClass="table table-bordered table-striped table-hover space-up" DataKeyNames="CARNE" EnableViewState="true" ClientIDMode="Static">
                                <Columns>

                                    <asp:BoundField DataField="CARNE" HeaderText="CARNE" ReadOnly="True" SortExpression="CARNE" />
                                    <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" SortExpression="NOMBRE" />
                                    <asp:BoundField DataField="APELLIDO1" HeaderText="APELLIDO1" SortExpression="APELLIDO1" />
                                    <asp:BoundField DataField="APELLIDO2" HeaderText="APELLIDO2" SortExpression="APELLIDO2" />

                                    <asp:BoundField DataField="CARRERA" HeaderText="CARRERA" SortExpression="CARRERA" />

                                </Columns>
                            </asp:GridView>
                        </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="linkBuscar" EventName="Click"/>
                            <asp:PostBackTrigger ControlID="gvRegistrados"/>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>

            </div>
                <div id="panel_B" class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Buscar</div>
                    </div>
                <div class="panel-body form-horizontal">
                  <div class="form-inline space-up space-down">
                        <asp:DropDownList ID="ddBusqueda" runat="server" CssClass="selectpicker form-control" Width="125px">
                            <asp:ListItem>Carné</asp:ListItem>
                            <asp:ListItem>Nombre</asp:ListItem>
                        </asp:DropDownList>
                   
                        <asp:TextBox ID="tbBuscar" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:LinkButton ID="linkBuscar" runat="server" CausesValidation="false" Text="<span class='glyphicon glyphicon-search'></span> Buscar " CssClass="btn btn-primary" OnClick="linkBuscar_Click"></asp:LinkButton>
                    </div>
                    <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional" RenderMode="Inline" ChildrenAsTriggers="true">
                        <ContentTemplate>
                        <div class="table-responsive">
                            <asp:GridView ID="gvRegistrados" runat="server"  OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnRowDataBound="GvCursor_RowDataBound" AutoGenerateColumns="False" CssClass="table table-bordered table-striped table-hover space-up" DataKeyNames="CARNE" EnableViewState="true" ClientIDMode="Static">
                                <Columns>

                                    <asp:BoundField DataField="CARNE" HeaderText="CARNE" ReadOnly="True" SortExpression="CARNE" />
                                    <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" SortExpression="NOMBRE" />
                                    <asp:BoundField DataField="APELLIDO1" HeaderText="APELLIDO1" SortExpression="APELLIDO1" />
                                    <asp:BoundField DataField="APELLIDO2" HeaderText="APELLIDO2" SortExpression="APELLIDO2" />

                                    <asp:BoundField DataField="CARRERA" HeaderText="CARRERA" SortExpression="CARRERA" />

                                </Columns>
                            </asp:GridView>
                        </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="linkBuscar" EventName="Click"/>
                            <asp:PostBackTrigger ControlID="gvRegistrados"/>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
              </div>
            </div>
            <script src="Scripts/bootstrap.min.js"></script>
        </div>
                      <div id="message" class="modal fade" role="dialog">
                        <div class="modal-dialog  ">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            
                            </div>
                            <div class="modal-body">

                               <h4 id="H1gg" runat="server" class="modal-title">No se tomará en cuenta la asignación de cocina, puesto que si se desabilita un estudiante no amerita llave de la misma</h4>
                            </div>
                            <div class="modal-footer">
      
                            
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        </div>
                   </div>
</asp:Content>

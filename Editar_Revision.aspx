﻿<%@ Page Title="Editar Revisión" Language="C#" MasterPageFile="~/Site.Master" 
    AutoEventWireup="true" 
    CodeBehind="Editar_Revision.aspx.cs" 
    Inherits="SICRER_Web_Application.Editar_Revision"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/Site.css" rel="Stylesheet" type="text/css" />
    <link href="Content/revisiones.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.8.3.min.js"></script>
    <link href="Content/jquery-ui-1.11.4.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.11.4.js"></script>
    <script src="Scripts/revisiones.js"></script>

    <div class="col-md-12 space-up">
        <div class="page-header">
            <asp:LinkButton ID="btnCerrar" runat="server" Text="<span class='glyphicon glyphicon-remove'></span> Cerrar" CssClass="btn btn-danger pull-right" style="margin-right:10px; border-radius:15px" OnClientClick="cerrar(); return false;"></asp:LinkButton>
            <h2 id="titulo" runat="server">Modificar Revisión</h2>
        </div>

        <!--Mensajes para el usuario-->
        <div>
             <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
                <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
                <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
                <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
                <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
                <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
                <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>
        </div>

        <div id="eliminado" runat="server">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Estudiantes</div>
                </div>
                <div class="panel-body form-horizontal form-inline">
                    <div class="col-md-7">
                        <div class="space-down">
                            <strong>Estudiantes Actuales: </strong>
                            <asp:Label ID="lbTipoRevision" runat="server"></asp:Label>
                            <asp:GridView ID="gvEstActuales" runat="server" 
                                CssClass="table table-bordered space-up" 
                                AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField DataField="NOMBRE" HeaderText="Nombre"/>
                                    <asp:BoundField DataField="CARNE" HeaderText="Carné"/>
                                </Columns>
                            </asp:GridView>
                        </div>

                        <div id="apartadoEstEditar" runat="server" class="space-down space-up">
                            <strong>Buscar Nuevos</strong><br/>
                            <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>                                
                                    <asp:DropDownList ID="ddResidencia" runat="server" DataTextField="Residencia" CssClass="form-control" 
                                        Width="130px"  AutoPostBack="true" OnSelectedIndexChanged="ddResidencia_SelectedIndexChanged">
                                        <asp:ListItem Text="Residencias" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>

                                    <asp:DropDownList ID="ddAla" runat="server" CssClass="form-control" Width="100px"
                                         AutoPostBack="true" OnSelectedIndexChanged="ddAla_SelectedIndexChanged">
                                        <asp:ListItem Text="Ala" Selected="True" Value="Ala"></asp:ListItem>
                                        <asp:ListItem Text="A"  Value="A"></asp:ListItem>
                                        <asp:ListItem Text="B"  Value="B"></asp:ListItem>
                                    </asp:DropDownList>

                                    <asp:DropDownList ID="ddCuarto" DataTextField="GRUPO_CUARTO" runat="server" CssClass="form-control" Width="140px"
                                         AutoPostBack="true">
                                        <asp:ListItem Text="Cuartos" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:LinkButton ID="btnConsultarNuevosEst" runat="server" Text="Consultar" CssClass="btn btn-primary" OnClick="btnConsultarNuevosEst_Click"></asp:LinkButton>
                                    <div class="space-up">
                                        <asp:Label ID="lbNoExistenEstConsul" runat="server" Text="" Visible="false" Font-Italic="true" Font-Bold="true" CssClass="space-up"></asp:Label>
                                    </div>

                                <asp:Panel ID="panelConsultaEst" runat="server" Visible="false" CssClass="space-up">
                                    <asp:GridView ID="gvEstConsultados" runat="server" 
                                        CssClass="table table-bordered space-down"
                                        AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundField DataField="NOMBRE_COMPLETO" HeaderText="Nombre"/>
                                            <asp:BoundField DataField="CARNE" HeaderText="Carné"/>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:CheckBox ID="ckBoxConfimarEst" runat="server" Text="Si, deseo incorporar estos nuevos estudiantes"/>
                                </asp:Panel>
                            </ContentTemplate>
                         </asp:UpdatePanel>
                     </div>
                   </div>
                </div>
            </div>
        
            <asp:UpdatePanel ID="updatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Calificación</div>
                    </div>
                    <div class="panel-body form-horizontal form-inline">
                        <div class="space-down">
                            <strong>Calificación Actual: </strong>
                            <asp:Label ID="lbCalifActual" runat="server"></asp:Label> 
                        </div>
                        <div class="table-responsive">       
                            <asp:GridView id="gvIndCalif" runat="server" AutoGenerateColumns="False" 
                                CssClass="table table-bordered table-hover space-up">
                                <Columns>
                                    <asp:BoundField  DataField="Indicador" HeaderText="Rubros de Calificación"/>

                                    <asp:TemplateField HeaderText="Excelente">
                                        <ItemTemplate>
                                            <asp:RadioButton ID="rbMBueno" runat="server" GroupName="groupCalif" Checked="true"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Bueno">
                                        <ItemTemplate>
                                            <asp:RadioButton ID="rbBueno" runat="server" GroupName="groupCalif" Checked="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Regular">
                                        <ItemTemplate>
                                            <asp:RadioButton ID="rbRegular" runat="server" GroupName="groupCalif" Checked="false"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Deficiente">
                                        <ItemTemplate>
                                            <asp:RadioButton ID="rbDeficiente" runat="server" GroupName="groupCalif" Checked="false"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                             </asp:GridView>
                         </div>
                        <asp:LinkButton ID="btnObtCalif" runat="server" Text="Obtener Calificación" CssClass="space-down btn btn-warning" Visible="false" OnClick="btnObtCalif_Click"  OnClientClick="loading('MainContent_imgCargando');"></asp:LinkButton>
                        <asp:Image ID='imgCargando' runat='server' ImageUrl='~/img/load_transaction.gif' CssClass="hide-ele"/>
                        <asp:Panel ID="panelNuevaCalif" runat="server" Visible="false" CssClass="space-up">
                            <div id="porcentaje" runat="server" class="space-down space-up porcentaje" visible="false">0%</div>
                            <div class="space-up">
                                <asp:CheckBox ID="ckBoxCalif" runat="server" Text="Si, deseo incorporar esta nueva calificación"/>
                            </div>
                        </asp:Panel>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Observaciones y Llamada Atención</div>
                    </div>
                    <div class="panel-body form-horizontal ">
                        <strong> Observaciones </strong>
                        <br />  
                        <asp:TextBox ID="textAreaObserv" runat="server" Height="150px" Width="60%" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>

                        <asp:Panel ID="panelLlamadaAten" runat="server" Visible="false" CssClass="space-up">
                            <strong>Llamada de Atención: </strong>
                            <asp:Label ID="lbLlamadaAten" runat="server" CssClass="space-down"></asp:Label>
                            <br />
                            <asp:CheckBox ID="ckBoxJustLlamadaAten" runat="server" Text="Justificar la llamada de Atención"/>
                            <br /><br />
                            <strong id="encabezadoNota" runat="server">Nota: </strong>
                            <asp:Label ID="lbNota" runat="server" CssClass="space-down">Si ha aceptado una nueva calificación, ignore esta llamada de atención.</asp:Label>
                        </asp:Panel>

                    </div>
                </div>
               </ContentTemplate>
            </asp:UpdatePanel>

            <div class="space-up space-down-footer">
                <div class="space-down">
                    <asp:Panel ID="panelMsjAdvActElim" runat="server" CssClass="alerta hide-ele">
                        <asp:Image ID="imgAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
                        <asp:Label ID="lbAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
                        <asp:LinkButton ID="btnConfirmarAct" runat="server" Text="Sí"  CssClass="btn btn-success btn-sm space-left hide-ele" OnClick="btnActualizar_Click" OnClientClick="ocultarElemento('MainContent_panelMsjAdvActElim');"/>
                        <asp:LinkButton ID="btnConfirmarElim" runat="server" Text="Sí"  CssClass="btn btn-success btn-sm space-left hide-ele" OnClick="btnEliminar_Click" OnClientClick="ocultarElemento('MainContent_panelMsjAdvActElim');"/>
                        <asp:LinkButton ID="btnCancelar" runat="server" Text="No" OnClientClick="ocultarElemento('MainContent_panelMsjAdvActElim'); return false;" CssClass="btn btn-primary btn-sm space-left"/>
                    </asp:Panel>
                </div>
                <button ID="btnAct" class="btn btn-warning"  onclick="loadEditar('MainContent_panelMsjAdvActElim','MainContent_btnConfirmarAct','MainContent_btnConfirmarElim','MainContent_lbAdv','1'); return false;"><span class='glyphicon glyphicon-edit'></span> Actualizar</button>
                <button ID="btnElim" class="btn btn-danger" onclick="loadEditar('MainContent_panelMsjAdvActElim','MainContent_btnConfirmarElim','MainContent_btnConfirmarAct','MainContent_lbAdv','2'); return false;"><span class='glyphicon glyphicon-remove-circle'></span>  Eliminar</button>
            </div>
       </div>
        <script src="Scripts/bootstrap.min.js"></script>
   </div>
</asp:Content>
